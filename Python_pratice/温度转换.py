#温度转换程序，华氏转摄氏温度，摄氏转华氏温度
temp_wendu = input('请输入带温度符号的温度值(c是摄氏温度，f是华氏温度)，退出请输入n：')
while temp_wendu not in ['n','N']:
    if temp_wendu[-1] in ['f','F']:
        c = (eval(temp_wendu[0:-1]) - 32) / 1.8
        print('转换后的摄氏温度是{:.2f}C'.format(c))
    elif temp_wendu[-1] in ['c','C']:
        f = eval(temp_wendu[0:-1]) * 1.8 + 32
        print('转换后的华氏温度是{:.2f}F'.format(f))
    else:
        print('输入错误,请重新输入，或输入n或N退出')
    temp_wendu = input('请输入带温度符号的温度值(c是摄氏温度，f是华氏温度)，退出请输入n：')
    
